import matplotlib.pyplot as plt
import os

directory = ''
all_files = []
combined_files = {}
maxes = {}
need_save = False


def extract_r(filename):
    """
    Extracts the R parameter from a filename
    :param filename: filename to be splitted
    :return: parameter R of this file
    """
    temp = filename.split(' ')
    while '' in temp:
        temp.remove('')
    output = temp[1][:-4].replace('D', 'E')
    return float(output)


def combine_files(f1, f2, r):
    """
    Combines data from file f1 and file f2 and stores in a dictionary with the key r
    :param f1: filename of thirst file
    :param f2: filename of the second file
    :param r: key for the dictionary
    :return: nothing
    """
    global combined_files
    with open(f1) as file1:
        data1 = file1.read()
    with open(f2) as file2:
        data2 = file2.read()
    combined_files[r] = data1 + data2
    return


def group_files():
    """
    Combines all pairs of files to a dict
    :return: nothing
    """
    global all_files
    for f1 in all_files:
        for f2 in all_files:
            r1 = extract_r(f1)
            r2 = extract_r(f2)
            if f1 != f2 and r1 == r2 and r1 not in combined_files:
                combine_files(f1, f2, r1)
                combined_files[r1] = combined_files[r1].split('\n')
                while '' in combined_files[r1]:
                    combined_files[r1].remove('')
    return


def read_files():
    """
    Reads all files in a directory with .dat extension
    :return: nothing
    """
    global all_files
    all_files = [directory + '\\' + f if directory != '' else f for f in os.listdir(directory or os.curdir) if f[-4:] == '.dat']
    return


def handle_line(line):
    """
    Converts a string to a list of numbers
    :param line: a line from a file to be converted
    :return: list of numbers
    """
    temp = line.replace('D', 'E').split(' ')
    while '' in temp:
        temp.remove('')
    output = [float(x) for x in temp]
    return output


def extract_first_and_second_column(data):
    """
    Extracts first and second column from a file
    :param data: array formed from a file
    :return: two lists of numbers
    """
    first_column = []
    second_column = []
    for d in data:
        processed = handle_line(d)
        first_column.append(processed[0])
        second_column.append(processed[1])
    return first_column, second_column


def plot_combined():
    """
    PLots dependency of second column from first column in all files
    :return: nothing
    """
    for k in combined_files:
        x, y = extract_first_and_second_column(combined_files[k])
        fig, ax = plt.subplots()
        ax.plot(x, y, lw=0.5)
        ax.set(xlabel='t', ylabel='\u03C8', title=f'R={k}')
        if need_save:
            name = str(k) + '.pdf'
            plt.savefig(name)
        plt.show()
    return


def extract_maximum(filename):
    """
    Extracts a maximum in second column in a given file
    :param filename: a file to be processed
    :return: a maximum in a second column
    """
    data = []
    with open(filename) as file:
        raw_data = file.read()
        pre_data = raw_data.split('\n')
        while '' in pre_data:
            pre_data.remove('')
        for d in pre_data:
            processed = handle_line(d)[1]
            data.append(processed)
    return max(data[int(len(data)/2):])


def plot_maximum():
    """
    PLots dependency of Psi_max from value of R in all files
    :return: nothing
    """
    psi2 = all_files[:int(len(all_files)/2)]
    x = []
    y = []
    for f in psi2:
        r = extract_r(f)
        m = extract_maximum(f)
        x.append(r)
        y.append(m)
    fig, ax = plt.subplots()
    ax.plot(x, y, 'bo-', lw=0.5)
    ax.set(xlabel='R', ylabel='\u03C8', title='Final')
    if need_save:
        plt.savefig('final.pdf')
    plt.show()
    return


def input_data():
    global directory, need_save
    directory = input('Введите путь к файлам: ')
    need_save = True if input('Сохранять ли файлы (Y/N): ') == 'Y' else False


def main():
    input_data()
    read_files()
    group_files()
    plot_combined()
    plot_maximum()
    return


if __name__ == '__main__':
    main()
