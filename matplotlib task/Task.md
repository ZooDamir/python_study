# Matplotlib

## Description

We have a *'.dat'* files with some data in them

Firstly we need to preprocess it as far as data is in a Fortran double precision form.

It is similar to exponential number format, but it uses D except of E.

Also, filenames are in a form **'psi2+G= [0-9]+D[+, -][0-9]+.dat'**. And data from files with the same parameter after equal sign must be combined.
