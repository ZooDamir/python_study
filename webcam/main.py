import cv2 as cv
from datetime import datetime

camera = cv.VideoCapture(0)
frame_width = int(camera.get(3))
frame_height = int(camera.get(4))
video_writer = cv.VideoWriter()

need_save = False

if not camera.isOpened():
    print("Can't get camera access")

while camera.isOpened():
    ret, frame = camera.read()
    if ret:
        if need_save:
            video_writer.write(frame)
        cv.imshow('Eto TI!!!', frame)
        key_pressed = cv.waitKey(1)
        if key_pressed == ord('q'):
            break
        elif key_pressed == ord('v'):
            need_save ^= True
            if need_save:
                video_writer = cv.VideoWriter(f'cam__{datetime.now().date()}_{datetime.now().time()}.avi',
                                              cv.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (frame_width, frame_height))
        elif key_pressed == ord('s'):
            cv.imwrite(f'TI_{datetime.now().date()}_{datetime.now().time()}.png', frame)
    else:
        break

camera.release()
video_writer.release()
cv.destroyAllWindows()
